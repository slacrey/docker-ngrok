# DOCKER NGROK IMAGE

## BUILD IMAGE

```linux
git clone https://gitee.com/slacrey/docker-ngrok.git
cd docker-ngrok
docker build -t slacrey/ngrok .
```

## 启动一个容器生成ngrok客户端,服务器端和CA证书
* you must mount your folder (E.g `/data/ngrok`) to container `/myfiles`
* if it is the first run, it will generate the binaries file and CA in your floder `/data/ngrok`
```
docker run --rm -it -e DOMAIN="tunnel.seelyn.com" -v ~/data/ngrok:/myfiles slacrey/ngrok /bin/sh /build.sh
```
当看到build ok !的时候就成功了

## 启动Ngrok server
* 直接挂载刚刚的/data/ngrok到容器即可启动服务
```linux
docker run -idt --name ngrok-server -v ~/data/ngrok:/myfiles -e DOMAIN='tunnel.seelyn.com' slacrey/ngrok /bin/sh /server.sh
docker run -idt --name ngrok-server -v ~/data/ngrok:/myfiles -p 80:80 -p 443:443 -p 4443:4443 -e DOMAIN='tunnel.seelyn.com' slacrey/ngrok /bin/sh /server.sh
```

## 域名解析
* 这样我们才能将 tunnel.seelyn.com 和 *.tunnel.seelyn.com DNS解析到我们的服务器

## 客户端连接

下载我们生成的客户端,我这里以osx为例,其他平台一样

首先创建一个ngrok.cfg配置文件

```
server_addr: "tunnel.seelyn.com:4443"
trust_host_root_certs: false
```
* 然后在命令行执行

```
./ngrok -config ./ngrok.cfg -subdomain wechat 192.168.99.100:80
```
我这里是将wechat.tunnel.seelyn.ccom绑定的本地192.168.99.100:80
如果不指定-subdomain参数,每次启动客户端的时候会随机分配一个域名,随意并不方便

